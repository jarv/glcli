# golang glcli
A PoC for using golang as a cli for gitlab, for fun...

## Install

* To install, use `go get`:

```
$ go get -d gitlab.com/jarv/glcli
```

* For the demo set your API token and UID

```
export GITLAB_API_TOKEN="********"
export GITLAB_UID="******"
```

## "Screenshot"

```
$ glcli list
+-----------------------------------------------------------------------------------------------+----------------------------------------------------------+
| Title                                                                                         | WebUrl                                                   |
+-----------------------------------------------------------------------------------------------+----------------------------------------------------------+
| admin accounts for Geo team on the Geo testbed                                                | https://gitlab.com/gitlab-com/infrastructure/issues/2666 |
| Discovery / PoC for provisioning multiple copies staging using terraform workspaces           | https://gitlab.com/gitlab-com/infrastructure/issues/2590 |
| Production readiness review for GEO                                                           | https://gitlab.com/gitlab-com/infrastructure/issues/2588 |
| Investigate getting pingdom events and deployments into prometheus for annotations in grafana | https://gitlab.com/gitlab-com/infrastructure/issues/2536 |
| Enable session affinity in HA proxy                                                           | https://gitlab.com/gitlab-com/infrastructure/issues/2527 |
+-----------------------------------------------------------------------------------------------+----------------------------------------------------------+
```

## Development setup

* Check out the vim plugin for golang https://github.com/fatih/vim-go
    - go fmt on save
    - completion
    - code navigation without tags

### References

* https://github.com/go-resty/resty - rest client for interracting with the gitlab api
* https://github.com/tcnksm/gcli - easy cli starter kit
* https://github.com/mitchellh/cli - same cli framework used for terraform, packer, etc.
* https://github.com/urfave/cli - other cli framework, used for this PoC
* https://github.com/apcera/termtables - pretty ascii tables

### Development

```
go get -d github.com/tcnksm/gcli
go get -u gopkg.in/resty.v0
go get -d github.com/urfave/cli
go get -u -v github.com/jteeuwen/go-bindata
go install github.com/jteeuwen/go-bindata
cd ~/go/src/github.com/jteeuwen/go-bindata/go-bindata
go build
cp go-bindata $GOPATH/bin
cd $GOPATH/src/github.com/tcnksm/gcli
make install
```

#### Create a new cli with a single "list" command

```
gcli new -F urfave_cli -c list -o=$USER -vcs=gitlab.com glcli
cd ~/go/src/gitlab.com/$USER/gcli
go build
```
## TODO

* More commands and tests

## Author

[jarv](https://gitlab.com/jarv)
