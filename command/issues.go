package command

type Issues []Issue

type Issue struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	WebUrl      string `json:"web_url"`
}
