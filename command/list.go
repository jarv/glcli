package command

import (
	"fmt"
	"github.com/apcera/termtables"
	"github.com/urfave/cli"
	"gopkg.in/resty.v0"
	"os"
)

func CheckResp(resp *resty.Response) {
	if resp.StatusCode() != 200 {
		fmt.Printf("\nBad resp: %v", resp.StatusCode())
		os.Exit(1)
	}
	/* Extra debug info
	fmt.Printf("\nResponse Status: %v", resp.Status())
	fmt.Printf("\nResponse Time: %v", resp.Time())
	fmt.Printf("\nResponse Recevied At: %v", resp.ReceivedAt())
	*/
}

func CmdList(c *cli.Context) error {
	issues := Issues{}
	resp, err := resty.R().
		SetQueryParams(map[string]string{
			"assignee_id": os.Getenv("GITLAB_UID"), // lookup and cache the id with users?username=jarv ?
			"state":       "opened",
		}).
		SetHeader("Accept", "application/json").
		SetHeader("PRIVATE-TOKEN", os.Getenv("GITLAB_API_TOKEN")).
		SetResult(&issues).
		Get("https://gitlab.com/api/v4/issues")

	if err != nil {
		fmt.Printf("\nUnable to request issues: %v", err)
		os.Exit(1)
	}
	CheckResp(resp)
	table := termtables.CreateTable()
	table.AddHeaders("Title", "WebUrl")
	for _, elem := range issues {
		table.AddRow(elem.Title, elem.WebUrl)
	}
	fmt.Println(table.Render())
	return nil
}
